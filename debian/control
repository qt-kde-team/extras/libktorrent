Source: libktorrent
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               dh-sequence-pkgkde-symbolshelper,
               cmake (>= 3.16~),
               extra-cmake-modules (>= 5.240.0~),
               gettext,
               libboost-dev (>= 1.71.0~),
               libgcrypt20-dev (>= 1.4.5~),
               libgmp-dev,
               libkf6archive-dev (>= 5.240.0~),
               libkf6config-dev (>= 5.240.0~),
               libkf6coreaddons-dev (>= 5.240.0~),
               libkf6crash-dev (>= 5.240.0~),
               libkf6i18n-dev (>= 5.240.0~),
               libkf6kio-dev (>= 5.240.0~),
               libkf6solid-dev (>= 5.240.0~),
               pkg-kde-tools,
               pkgconf,
               qt6-5compat-dev,
               qt6-base-dev (>= 6.5.0~),
               xauth <!nocheck>,
               xvfb <!nocheck>,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://www.kde.org/applications/internet/ktorrent/
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/libktorrent.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/libktorrent

Package: libktorrent6-6
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends},
Suggests: libktorrent-l10n (>= ${source:Version}),
Description: KTorrent library for C++ / Qt 5 / KDE Frameworks
 The KTorrent library is a C++ / Qt 5 / KDE Frameworks based implementation of
 the BitTorrent protocol (mostly client side).
 .
 The library supports connectivity to HTTP and UDP trackers, mainline DHT and
 the new generation Micro Transport Protocol (uTP). In addition, it provides
 many powerful BitTorrent network features including but not limited to torrent
 downloading and seeding, torrent creation and downloaded data verification,
 magnet links, advanced peer management, IP blocking lists.

Package: libktorrent-dev
Architecture: any
Section: libdevel
Depends: libboost-dev (>= 1.71.0~),
         libgcrypt20-dev (>= 1.4.5~),
         libgmp-dev,
         libktorrent6-6 (= ${binary:Version}),
         ${misc:Depends},
         ${sameVersionDep:libkf6archive-dev},
         ${sameVersionDep:libkf6config-dev},
         ${sameVersionDep:libkf6coreaddons-dev},
         ${sameVersionDep:libkf6i18n-dev},
         ${sameVersionDep:libkf6kio-dev},
         ${sameVersionDep:qt6-base-dev},
Description: development files for the KTorrent Library
 The KTorrent library is a C++ / Qt 5 / KDE Frameworks based implementation of
 the BitTorrent protocol (mostly client side).
 .
 This package contains header files, CMake modules and other files needed for
 developing and compiling/linking which use the KTorrent library.

Package: libktorrent-l10n
Architecture: all
Section: localization
Depends: ${misc:Depends},
Description: localization files for the KTorrent library
 The KTorrent library is a C++ / Qt 5 / KDE Frameworks based implementation of
 the BitTorrent protocol (mostly client side).
 .
 This package contains translations of the KTorrent library.
